package com.stuffdot.server.dto;

import lombok.Data;

@Data
public class PioResponse {

    private String eventId;
}
