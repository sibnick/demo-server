package com.stuffdot.server.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PioEvent {
    private String event;
    private String entityType;
    private String entityId;
    private Map<String, Object> properties;
    private Date eventTime;
}
