package com.stuffdot.server.dto;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;

@Data
public class Movie {
    @Id
    private String id;

    @Indexed(direction = IndexDirection.ASCENDING)
    private String title;

    private String description;

    private int releaseYear;
}
