package com.stuffdot.server.event;

import com.stuffdot.server.dto.Movie;
import com.stuffdot.server.dto.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UserCascadeSaveMongoEventListener extends AbstractMongoEventListener<User> {

    @Override
    public void onAfterSave(AfterSaveEvent<User> event) {
        log.info("User saved: {}", event.getSource());
    }

    
}