package com.stuffdot.server.event;

import com.stuffdot.server.dto.Movie;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterDeleteEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.stereotype.Component;

import java.util.Date;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

@Slf4j
@Component
@Lazy

public class MovieCascadeSaveMongoEventListener extends AbstractMongoEventListener<Movie> {


    private Client elastic;

    @Autowired
    public MovieCascadeSaveMongoEventListener(Client elastic) {
        this.elastic = elastic;
    }

    @Override
    @SneakyThrows
    public void onAfterSave(AfterSaveEvent<Movie> event) {
        Movie movie = event.getSource();
        log.info("Movie saved: {}", movie.getId());
        val data = jsonBuilder()
                .startObject()
                .field("keywords", movie.getTitle()+ " "+ movie.getDescription())
                .endObject();
        IndexResponse response = elastic
                .prepareIndex("movies", "movie", movie.getId())
                .setSource(data)
                .get();
        log.info("Movie saved: {} to elastic too {}", movie.getId(), response.status());
    }


    @Override
    public void onAfterDelete(AfterDeleteEvent<Movie> event) {
        elastic.prepareDelete("movies", "movie",""+ event.getDBObject().get("_id"));
    }
}