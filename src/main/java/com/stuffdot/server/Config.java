package com.stuffdot.server;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix="stuffdot")
@Data
@Configuration
@Component
public class Config {

    private String pioUrl;
    private String elasticHost;
    private int elasticPort;
}
