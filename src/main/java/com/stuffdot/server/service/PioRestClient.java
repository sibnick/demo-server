package com.stuffdot.server.service;

import com.stuffdot.server.Config;
import com.stuffdot.server.dto.PioEvent;
import com.stuffdot.server.dto.PioResponse;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class PioRestClient {

    private Config cfg;

    @Autowired
    public PioRestClient(Config cfg) {
        this.cfg = cfg;
    }

    public boolean sendEvent(PioEvent event){

        RestTemplate rt = new RestTemplate();
        try {
            val rs = rt.postForObject(cfg.getPioUrl(), event, PioResponse.class);
            log.debug("Saved event: {}", rs);
            return true;
        } catch (RestClientException e) {
            log.warn("", e);
        }
        return false;
    }



}
