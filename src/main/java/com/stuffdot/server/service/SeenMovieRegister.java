package com.stuffdot.server.service;


import com.stuffdot.server.dto.PioEvent;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@RestController
@RequestMapping(path = "/service/register/")
@Slf4j
public class SeenMovieRegister {

    private UserRepository userRepository;
    private MovieRepository movieRepository;
    private PioRestClient pioClient;

    @Autowired
    public SeenMovieRegister(UserRepository userRepository, MovieRepository movieRepository, PioRestClient pioClient) {
        this.userRepository = userRepository;
        this.movieRepository = movieRepository;
        this.pioClient = pioClient;
    }

    @RequestMapping(value = "/movie/{id}", method = RequestMethod.POST)
    public ResponseEntity<String> registerMovie(@PathVariable("id") String userId, @RequestParam("movieId") String movieId){
        log.debug("Try register userId / movieId : {} / {}", userId, movieId);
        //find user
        val user = userRepository.findOne(userId);
        if(user==null){
            return ResponseEntity.badRequest().body("User not found");
        }
        //check movie id
        val movie = movieRepository.findOne(movieId);
        if(movie==null){
            return ResponseEntity.badRequest().body("Movie not found");
        }
        user.getSeenMovies().add(movie.getId());
        userRepository.save(user);
        log.debug("Saved to Mongo");
        pioClient.sendEvent(new PioEvent("my_event", "seen", userId+"|"+movieId, null, null));
        log.debug("Posted to PIO");
        return ResponseEntity.ok("OK");
    }
}
