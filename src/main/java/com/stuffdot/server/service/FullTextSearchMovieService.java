package com.stuffdot.server.service;

import com.stuffdot.server.dto.Movie;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.io.stream.BytesStreamOutput;
import org.elasticsearch.common.io.stream.StreamOutput;
import org.elasticsearch.common.xcontent.ToXContent;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.index.query.InnerHitBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryParseContext;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping(path = "/service/search/")
@Slf4j
public class FullTextSearchMovieService {

    private Client elastic;

    @Autowired
    public FullTextSearchMovieService(Client elastic) {
        this.elastic = elastic;
    }


    @RequestMapping(value = "/movies", method = RequestMethod.GET)
    @SneakyThrows
    public void seek(HttpServletResponse response, @RequestParam("q") String query,
                     @RequestParam(value = "from", defaultValue = "0") int from,
                     @RequestParam(value = "size", defaultValue = "10") int size)
    {

        HighlightBuilder highlight = SearchSourceBuilder.highlight()
                .field("keywords").highlighterType("plain")
                .order("score").preTags("<b>").postTags("</b>");

        SearchResponse rs = elastic.prepareSearch("movies")
                .setTypes("movie")
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setQuery(QueryBuilders.termQuery("keywords", query))
                //.setPostFilter(QueryBuilders.rangeQuery("releaseYear").from().to())
                .addStoredField("id")
                .addStoredField("keywords")
                .highlighter(highlight)
                .setFrom(from)
                .setSize(size)
                .setExplain(false)
                .get();

        val builder = XContentFactory.jsonBuilder();
        rs.toXContent(builder, ToXContent.EMPTY_PARAMS);
        response.setContentType("application/json;charset=UTF-8");
        builder.bytes().writeTo(response.getOutputStream());
    }
}
