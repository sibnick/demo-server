package com.stuffdot.server.service;

import com.stuffdot.server.dto.Movie;
import com.stuffdot.server.dto.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(collectionResourceRel = "movie", path = "movie")
public interface MovieRepository extends MongoRepository<Movie, String>{
    //
}