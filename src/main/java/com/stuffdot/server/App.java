package com.stuffdot.server;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.node.Node;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;


@Configuration
@EnableConfigurationProperties(Config.class)
@SpringBootApplication
@Slf4j
public class App implements CommandLineRunner{

    private volatile Client client;
    private static final Object lock = new Object();

    private Config cfg;

    @Autowired
    public App(Config cfg){
        this.cfg = cfg;
        log.info("Cfg: {}", cfg);
    }


    @Override
    @SneakyThrows
    public void run(String... args) {
        log.info("Started");
    }


    private void initTypeAndIndex(TransportClient client) {
        CreateIndexRequest indexRequest = new CreateIndexRequest().index("movies");
        val rs = client.admin().indices().create(indexRequest);
        try {
            log.info("Prepare movies index: {}", rs.get().isAcknowledged());
        }catch (Exception e){
            log.info("Prepare movies index failed", e);
        }

    }

    @Bean({"elastic"})
    @SneakyThrows
    public Client getESClient(){
        if(client==null){
            synchronized (lock){
                if(client==null) {
                    //todo move to app props
                    InetSocketTransportAddress transportAddress = new InetSocketTransportAddress(InetAddress.getByName(cfg.getElasticHost()), cfg.getElasticPort());
                    Settings settings = Settings.builder()
                            .put("client.transport.sniff", true)
                            .put("cluster.name", "stuffdot-cluster")
                            .build();

                    val client = new PreBuiltTransportClient(settings).addTransportAddress(transportAddress);
                    initTypeAndIndex(client);
                    this.client = client;
                }
            }
        }
        return client;
    }

    public static void main(String[] args) throws Exception {

        SpringApplication.run(App.class, args);
    }
}
